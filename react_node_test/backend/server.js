const express = require('express')
const cors = require('cors')
const fs = require('fs')

const app = express()

const port = 3001;
app.use(cors());

app.get('/api/movies',(request,response)=>{
    const moviesData = JSON.parse(fs.readFileSync('movies.json'))
    response.json(moviesData)
})
app.get('/api/movies/:id',(request,response)=>{
    const movieId= parseInt(request.params.id)
    const moviedata = fs.readFileSync()
    const movies = json.parse(movieData)
    const movie = movies.find(movie => movie.id==movieId)
    if(movie){
        response.json(movie)
    }
    else{
        return response.status(500).json({Error:"movie not found"})
    }
})

app.listen(port,()=>{
    console.log('server is runing on 3002')
})