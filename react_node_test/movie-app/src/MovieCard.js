import React, { useState,useEffect } from 'react';
import {useParams} from "react-router-dom";
import './MovieCard.css';
const MovieCard = ({}) =>{
    const {id} = useParams();
    const [movie,setMovie] = useState(null)
    useEffect(()=> {
        fetch(`http://localhost:3002/api/movies/${id}`).then(response=>response.json())
        .then(data=>setMovie(data))
      },[id])
    return(
        <div className='movie-card'>
            <h2>{movie.name}</h2>
            <p> rating :{movie.rating}</p>
            <p>releaseDat :{movie.releaseDate}</p>
        </div> 
    )
}
export default MovieCard;