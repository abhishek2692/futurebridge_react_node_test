import react,{useState,useEffect} from 'react';
import {
  BrowserRouter as Router, Route, Routes
} from "react-router-dom";
import MovieList from './MovieList';
import MovieCard from './MovieCard';

function App(){
  return(
    <Router>
  <div className="App">
    <Routes>
      <Route exact path="/"element={<MovieList/>} />
      <Route exact path="/movies/:id" element={<MovieCard/>} />
    </Routes>
  </div>
  </Router>
  )
}
export default App;
