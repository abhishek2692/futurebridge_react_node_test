import react,{useState,useEffect} from 'react';
import Link from "react-router-dom";
import MovieCard from './MovieCard';
const MovieList = () =>{
    const [movies,setMovies] = useState([])
    useEffect(()=> {
      fetch('http://localhost:3002/api/movies').then(response=>response.json())
      .then(data=>setMovies(data))
    },[])

  

  return(
    <div>
      <h2>Movie List</h2>
      <ul className='movie-list'>
      {movies.map(movie=>(
        <li key={movie.id}>
        <link to={`/movie/${movie.id}`}>  
            {movie.name} 
        </link>
        </li>
        ))}
      </ul>
    </div>
  )

}
export default MovieList;
