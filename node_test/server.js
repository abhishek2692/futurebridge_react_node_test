const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs/promises')
const app = express()

const port = 3000;
app.use(bodyParser.json());
const users=[]
const usersFile = 'users.json'
class User{ constructor (firstName, lastName,email,contact){
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
        this.contact = contact
    }
}

app.get('/users',async (request,response)=>{
    try{
        const data = await fs.readFile(usersFile)
        const users = json.parse(data)
        response.json(users)
    }
    catch(Error){
        return response.status(500).json({Error:"failed to retrive users."})
    }
})

app.post('/users',async(request,response)=>{
    
    const {firstName, lastName,email,contact} = request.body    
    if(!firstName || !lastName || !email || !contact){
        return response.status(400).json({Error:"one or more fields are missing."})
    }

    const newUser = new User(firstName, lastName,email,contact);
    try{
        const data = await fs.readFile(usersFile)
        const users = json.parse(data)   
        users.push(newUser);
        await fs.writeFile(usersFile,JSON.stringify(users))
        response.status(201).json(newUser)
    }
    catch(Error){
        return response.status(500).json({Error:"failed to create user."})
    }
})

app.put('/users/:index', async(request,response)=>{
    const index = parseInt(request.params.index)
    if(!index){
        return response.status(400).json({Error:"invalid index"})
    }
    try{
        const data = await fs.readFile(usersFile)
        const users = json.parse(data)
        const {firstName, lastName,email,contact} = request.body    
        if(!firstName || !lastName || !email || !contact){
            return response.status(400).json({Error:"one or more fields are missing."})
        }
        const updatedUser = new User(firstName, lastName,email,contact);
        users[index] = updatedUser
        await fs.writeFile(usersFile,JSON.stringify(users))
        response.status(201).json(updatedUser)
    }
    catch(Error){
        return response.status(500).json({Error:"failed to update user."})
    }
})
app.delete('/users',async(request,response)=>{
    const index = parseInt(request.params.index)
    if(!index){
        return response.status(400).json({Error:"invalid index"})
    }
    try{
        const data = await fs.readFile(usersFile)
        const users = json.parse(data)
        if(index<0 || index>=users.length){
            return response.status(400).json({Error:"user not found"})
        }
        users.splice(index,1)
        await fs.writeFile(usersFile,JSON.stringify(users))
        response.status(201).send();
    }
    catch(Error){
        return response.status(500).json({Error:"failed to delete user."})
    }
})

app.listen(port,()=>{
    console.log('server is runing on 3000')
})